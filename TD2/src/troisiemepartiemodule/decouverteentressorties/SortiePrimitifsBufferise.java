/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouverteentressorties;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author palmaa
 */
public class SortiePrimitifsBufferise {
    
    public static void main(String[] args) {
        DataOutputStream dos=null;
        try {

            dos = new DataOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream("ficprimitif")
                    )
            );
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
            System.exit(-1);
        }
         try {
            dos.writeInt(43712);
            dos.writeDouble(3.14);
                   
            
        } catch (IOException ex) {
            System.err.println(ex);
            System.exit(-2);
        }
        try {
            dos.flush();
            dos.close();
                   
            
        } catch (IOException ex) {
            System.err.println(ex);
            System.exit(-3);
        }
    }
    
    
}
