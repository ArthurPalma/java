/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouverteentressorties;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author palmaa
 */
public class SortieDeBase {
    
    public static void main(String[] args) {
    
        FileOutputStream fos=null;
        
        try{
            fos =new FileOutputStream("Fic");
        }catch (FileNotFoundException ex){
             System.err.println(ex);
             System.exit(-1);
        }
        
        try {
            fos.write(12);
        } catch (IOException ex) {
              System.err.println(ex);
             System.exit(-2);
        }
        
        try {
            fos.close();
        } catch (IOException ex) {
             System.err.println(ex);
             System.exit(-3);
        }
    }
    
}
