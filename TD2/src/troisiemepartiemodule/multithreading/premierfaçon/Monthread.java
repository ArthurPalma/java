/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.multithreading.premierfaçon;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author palmaa
 */
public class Monthread extends Thread{
    
    private String message;

    private boolean continuer;
    public Monthread(String message) {
        this.message = message;
    }


    public void run(){
    
          int compteur=0;
          
          continuer=true;
          while(continuer){
                System.out.println(message+":"+compteur);
                  
                compteur++;
                  
              try {

                  Thread.sleep(500);
              } catch (InterruptedException ex) {
              }
          }
    
    }
    
    public void arreter(){
        continuer=false;  
    }
    
    
    
}
