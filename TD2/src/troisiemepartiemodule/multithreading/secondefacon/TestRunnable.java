/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.multithreading.secondefacon;

/**
 *
 * @author palmaa
 */
public class TestRunnable {
    
        public static void main(String[] args){
        
            System.out.println("debut");
            
            MonRunnable r1=new MonRunnable("toto");
            MonRunnable r2=new MonRunnable("           titi");
            
            Thread t1=new Thread(r1);
            Thread t2=new Thread(r2);
            
            t1.start();
            t2.start();
            System.out.println("fin");
    }
    
}
