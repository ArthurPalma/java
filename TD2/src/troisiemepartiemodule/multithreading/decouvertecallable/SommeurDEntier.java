/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.multithreading.decouvertecallable;

import java.util.concurrent.Callable;

/**
 *
 * @author palmaa
 */
public class SommeurDEntier implements Callable<Integer>{

    private int n;
    public SommeurDEntier(int n){
        this.n=n;
}
    @Override
    public Integer call() throws Exception {
        System.out.println("    -debut call"+n+":"+Thread.currentThread().getName());
        
        Integer resultat=0;
        
        for(int i=0; i<n; i++)
        {
        resultat+=i;
        System.out.println("      "+resultat+" "+Thread.currentThread().getName());
        
        Thread.sleep(750);
        }
        System.out.println("    -fin call"+n+":"+Thread.currentThread().getName());
        return resultat;
    }
    
}
