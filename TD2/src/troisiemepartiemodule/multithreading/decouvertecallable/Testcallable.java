/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.multithreading.decouvertecallable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author palmaa
 */
public class Testcallable {
    public static void main(String[] args){
        
    System.out.println("debut de main");
    
    SommeurDEntier somme10=new SommeurDEntier(10);
    SommeurDEntier somme25=new SommeurDEntier(25);
    
    ExecutorService monExecuterService;
    
    monExecuterService=Executors.newFixedThreadPool(3);
    
    Future<Integer> res25=monExecuterService.submit(somme25);
    
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
           
        }
        Future<Integer> res10=monExecuterService.submit(somme10);
        
        boolean continuer=true;
        
        while (continuer){
            
            if (res25.isDone())
                continuer=false;
            else
                try {
                    Thread.sleep(300);
            } catch (InterruptedException ex) {
            }
        }
        
        try {
            Integer leres25=res25.get();
            System.out.println(leres25);
        } catch (InterruptedException ex) {
        } catch (ExecutionException ex) {
        }
        System.out.println("fin");
        monExecuterService.shutdown();
    }


    
}
