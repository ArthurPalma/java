/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TD3.DiscoverIHM;

import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

/**
 *
 * @author Louis
 */
public class myPanel extends Panel{
    private Button myButton;
    private Label myLabel;
    public myPanel(){
        myButton=new Button("Click here");
        myLabel=new Label("Number of clicks");
        setLayout(new GridLayout(2,1));
        add(myButton);
        add(myLabel);
    }
}
