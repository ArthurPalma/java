/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.informaticien;

import secondepartiemodule.Geometrie.Point;
import java.awt.Color;

/**
 *
 * @author palmaa
 */
public class Pixel extends Point {

    private Color couleur;
    private boolean visible;

    // et quand on utilise super, c'est nécessairement la première action, sinon ça fail
    public Pixel(Color couleur, boolean visible, int abscisse, int ordonnee) {
        super(abscisse, ordonnee);
        this.couleur = (couleur == null) ? Color.BLUE : couleur;
        
        this.visible=false;
        if (visible) {
            allumeToi();
        }
    }

    public Pixel() {
        this(null, false, 0, 0);
    }

    public void allumeToi() {
        if (!visible) {
            // faudrait allumer le pixel ici

            System.out.println("le pixel est devenu visible");
            visible = true;
        }
    }

    public void eteinsToi() {
        if (visible) {
            // faudrait éteindre le pixel ici

            System.out.println("le pixel est devenu invisible");
            visible = false;
        }

    }

    @Override
    public String toString() {
        return "Pixel{" + "couleur=" + couleur
                + ", visible=" + visible
                + "," + super.toString()
                + '}';
    }

    public void deplacetoi(int x, int y) {

        boolean etatinitial = visible;
        if (visible) {
            this.eteinsToi();
        }
        super.deplacetoi(x, y);
        if (etatinitial) {
            this.allumeToi();
        }
    }

    public void deplacetoi (Point ou){
        
        if(ou==null) return;
        deplacetoi(ou.GetAbs(), ou.GetOrd());
    }
    
}
