/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.test;

import secondepartiemodule.informaticien.Pixel;

import secondepartiemodule.Geometrie.Point;

/**
 *
 * @author palmaa
 */
public class Testpixel {

    public static void main(String[] args) {
        Pixel pix1 = new Pixel();

        System.out.println(pix1);

        pix1.allumeToi();
        System.out.println(pix1);
        pix1.deplacetoi(2, 2);

        System.out.println(pix1);

        Point p;

        p = pix1;

        p.deplacetoi(0, 0);

        p = new Point();

        System.out.println(p.getClass().getSuperclass().getName());

        p = new Point(1, 3);
        p.deplacetoi(4, 4);

        p = new Pixel();
        p= pix1;
        
        p.deplacetoi(4, 4);
    }

}
