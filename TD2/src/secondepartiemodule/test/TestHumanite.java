/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.test;
import secondepartiemodule.abstraction.EtreHumain;
import secondepartiemodule.abstraction.Femme;
import secondepartiemodule.abstraction.Homme;

/**
 *
 * @author palmaa
 */
public class TestHumanite {
    
   public static void main(String[] args) {
       
       Femme f =new Femme("Simone");
       Homme h =new Homme("Simon");
       System.out.println(f);
       System.out.println(h);
       
       System.out.println(f.getNomDeJeuneFille());
       System.out.println(h.esTuBarbu());
       
       f.QuelEstTonNom();
       
       System.out.println("----------------");
       
       EtreHumain e;
       
       // e= new EtreHumain(); compile pas car méthode abstraite peut pas instancier
       
       e=f;
       
       // e.getNomDeJeuneFille(); logique, tous les êtres humains ne sont pas des femmes
       
       
       e.vaTAmuser();
       
       e=h;
       
       e.vaTAmuser();
       
       System.out.println(e.getClass().getName());
       System.out.println(e.getClass().getSuperclass());
       System.out.println(e.getClass().getSuperclass().getSuperclass());
       System.out.println(e.getClass().getSuperclass().getSuperclass().getSuperclass());
       
       
 
   }
 }
    

