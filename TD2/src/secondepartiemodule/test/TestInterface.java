/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.test;
import java.util.Arrays;
import java.util.Comparator;
import secondepartiemodule.decouverteinterface.Lampe;
import secondepartiemodule.decouverteinterface.Lecteur;
import secondepartiemodule.decouverteinterface.LecteurDVD;
import secondepartiemodule.decouverteinterface.Vieuxcomparateur;
import secondepartiemodule.decouverteinterface.demarrable;

/**
 *
 * @author palmaa
 */
public class TestInterface {
    
    public static void main(String[] args)
    {
       Lampe laLampe=new Lampe(100);
       laLampe.getPuissance();
       System.out.println(laLampe);
       laLampe.on();
       
       LecteurDVD Lecteursony= new LecteurDVD("sony");
       System.out.println(Lecteursony);
       Lecteursony.on();
       Lecteursony.play();
       
       System.out.println("---------------------");
       
       demarrable d;
       
       d= laLampe;
       
       d.on();
       Lecteur lelecteur=Lecteursony;
       lelecteur.play();
       
       System.out.println("---------------------");
       
       Lampe[] deslampes={new Lampe(150),new Lampe(15),new Lampe(50)};
       
       System.out.println("avant le tri");

       for(Lampe unelampe : deslampes)
       {
           System.out.println(unelampe);
       }
       Arrays.sort(deslampes);
       System.out.println("apres le tri");
       for(Lampe unelampe : deslampes)
       {
           System.out.println(unelampe);
       }
       
        System.out.println("---------------------");
        
        LecteurDVD[] deslecteursdvds={new LecteurDVD("sony"),new LecteurDVD("moulinex"),new LecteurDVD("samsung")};
        
        System.out.println("avant le tri");
        for(LecteurDVD unlecteurdvd : deslecteursdvds)
        {
            System.out.println(unlecteurdvd);
        
        }
        Vieuxcomparateur comparateur= new Vieuxcomparateur();
       
        Arrays.sort(deslecteursdvds, comparateur);
        System.out.println("après le tri");
        for(LecteurDVD unlecteurdvd : deslecteursdvds)
        {
            System.out.println(unlecteurdvd);
        
        }

        System.out.println("---------------------");
        
        Comparator<LecteurDVD> moderne = new Comparator<LecteurDVD>() {
           @Override
           public int compare(LecteurDVD arg0, LecteurDVD arg1) {
              return arg1.getMarque().compareTo(arg0.getMarque()); 
           }
        };
        
        Arrays.sort(deslecteursdvds, moderne);
        System.out.println("après le tri");
        for(LecteurDVD unlecteurdvd : deslecteursdvds)
        {
            System.out.println(unlecteurdvd);
        
        }
        
        System.out.println("*******************************");
        Arrays.sort(deslecteursdvds, new Comparator<LecteurDVD>(){
           @Override
           public int compare(LecteurDVD arg0, LecteurDVD arg1) {
            
              return arg0.getMarque().compareTo(arg1.getMarque()); 
           
           }
        });
    
   }
}