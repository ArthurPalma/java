/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.decouverteinterface;

/**
 *
 * @author palmaa
 */
public class Lampe implements demarrable, Comparable<Lampe> {
    
    // type lampe, un objet
    private int puissance;

    public Lampe(int puissance) {
        this.puissance = puissance;
    }

    public int getPuissance() {
        return puissance;
    }

    @Override
    public String toString() {
        return "Lampe{" + "puissance=" + puissance + '}';
    }   

    @Override
    public void on() {
        System.out.println("je suis une lampe qui s'allume"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void off() {
        System.out.println("je suis une lampe qui s'eteind");  //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Lampe arg0) {
        return puissance-arg0.puissance; //To change body of generated methods, choose Tools | Templates.
    }

    
}
