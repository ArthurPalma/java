/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.decouverteinterface;

/**
 *
 * @author palmaa
 */
public class LecteurDVD implements demarrable, Lecteur {
    
    private String marque;

    public LecteurDVD(String marque) {
        this.marque = marque;
    }

    public String getMarque() {
        return marque;
    }

    @Override
    public String toString() {
        return "LecteurDVD{" + "marque=" + marque + '}';
    }

    @Override
    public void on() {
        System.out.println("je suis une lampe qui s'allume"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void off() {
        System.out.println("je suis une lampe qui s'eteind"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void play() {
        System.out.println("je suis un lecteur dvd en lecture"); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void stop() {
        System.out.println("je suis un lecteur dvd a l'arret"); //To change body of generated methods, choose Tools | Templates.
    }
   
}
