/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.abstraction;

/**
 *
 * @author palmaa
 */
public abstract class EtreHumain{
    
    private String nom;
    

    public EtreHumain(String nom) {
        this.nom=nom;
    }
    
    public EtreHumain() {
          this.nom=("nom par défaut");
    }
    public String QuelEstTonNom(){
        return this.nom;
    }

    @Override
    public String toString() {
        return "EtreHumain{" + "nom=" + nom + '}';
    }
    
    public abstract void vaTAmuser();
    
}
