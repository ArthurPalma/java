/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.abstraction;

/**
 *
 * @author palmaa
 */
public class Homme extends EtreHumain{
    
    private boolean barbu;
    
    public Homme(String nom){
        super(nom);
        this.barbu=false;
    
    }
    
    public boolean esTuBarbu(){
           return barbu;
    }

    @Override
    public String toString() {
        return "Homme{" + "barbu=" + "-"+ barbu + super.toString() +'}';
    }

    @Override
    public void vaTAmuser() {
       System.out.println(QuelEstTonNom()+" fait le menage.");
    }
    
    
}
