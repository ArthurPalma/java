/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decouverteIHM;

import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Louis
 */
public class myPanelarchive extends Panel implements ActionListener{
    private Button myButton;
    private Label myLabel;
    private int compteur;
    public myPanelarchive(){
        myButton=new Button("Click here");
        myLabel=new Label("Number of clicks");
        setLayout(new GridLayout(2,1));
        add(myButton);
        add(myLabel);
        
        compteur=0;
        
        //myButton.addActionListener(new MonEcouteur(this));
        myButton.addActionListener(this);
    }

    public void YA_EU_UN_CLICK() {
         compteur++;
         
         lelabelupdate("tu as click"+compteur+"fois");
    }
    
    public void lelabelupdate(String text)
    {
         myLabel.setText(text);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        YA_EU_UN_CLICK();
    }
}
