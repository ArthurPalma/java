/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decouverteIHM;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author palmaa
 */
public class MonEcouteur implements ActionListener {

    private myPanel monpanneau;

    public MonEcouteur(myPanel monpanneau) {
        this.monpanneau = monpanneau;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        monpanneau.YA_EU_UN_CLICK();
    }
    
    
}
