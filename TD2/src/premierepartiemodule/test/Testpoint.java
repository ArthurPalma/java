/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premierepartiemodule.test;

import premierepartiemodule.Geometrie.Point;
import premierepartiemodule.Geometrie.Droite;


/**
 *
 * @author palmaa
 */
public class Testpoint {
    public static void main(String[] args)
    {
         Point p1;
         
         p1=new Point(3,4);
         //p1.affichetoi();
         
         System.out.println("avant deplacement");
         
         p1.deplacetoi(1, 2);
         //p1.affichetoi();
         
         Point p2=new Point();
         
         System.out.println(p2);
         
         p1.deplacetoi(p2);
         //p1.affichetoi();
         
         System.out.println("le point p2 : "+p2);

         
         System.out.println("apres deplacement");
         
         //TP3
         
         Point p4,p5;
         
         p4 = new Point(0,0);
         p5 = new Point(3,4);
         
         Droite d1,d2;
         
         d1= new Droite(p4,p5);
         d2= new Droite(p2,p4);
         
         
         d2.deplacetoi(d1);
         
         System.out.println("la droite d1 :"+d1);
         System.out.println("la droite d2 :"+d2);
         d2.deplacetoi(p5, p1);
         System.out.println("la droite d2 :"+d2);
    }
    
}
