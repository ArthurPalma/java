/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premierepartiemodule.test;

import premierepartiemodule.Geometrie.Droite;
import premierepartiemodule.Geometrie.Point;
/**
 *
 * @author palmaa
 */
public class Testdroite {
    
    public static void main(String[] args)
    {
        Point p1;
        
        p1=new Point();
        Point p2= new Point(1,2);
        
        Droite d = new Droite(p1,p2);
        
       System.out.println("p1="+p1);
       System.out.println("p2="+p2);
       System.out.println("d="+d);
       
       //p2.deplacetoi(0, 0);
       
       d.getP2().deplacetoi(p1);
       System.out.println("p2="+p2);
       System.out.println("d="+d);
        
        
    
    }
    
}
