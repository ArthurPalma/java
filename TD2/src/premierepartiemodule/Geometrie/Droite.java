/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premierepartiemodule.Geometrie;

/**
 *
 * @author palmaa
 */
public class Droite {
    private Point p1;
    private Point p2;
    
    public Droite(Point p1, Point p2)
    {
        // constructeur, retourne rien même pas void
        
        
        if(!((p1==null)||(p2==null)||p1.esTuIdentiqueA(p2)))
        {
           this.p1=new Point(p1.GetAbs(),p1.GetOrd());
           this.p2=new Point(p2.GetAbs(),p2.GetOrd());
        
        }else{
           this.p1=new Point();
           this.p2=new Point(1,1);
        }
    }
    
    public Droite(int abscisse1, int ordonnee1, int abscisse2, int ordonnee2)
    {
        // constructeur, retourne rien même pas void
        this(new Point(abscisse1, ordonnee1),new Point(abscisse2, ordonnee2));
    }
    
        public Droite()
    {
        // un seul this et en début de ligne
        // constructeur, retourne rien même pas void
        //this(0,0,1,1);
        //this(new Point(0, 0),new Point(2,2));
        this(null, null);
    }

    public Point getP1() {
        return new Point(p1);
    }

    public void setP1(Point p1) {
        if(p1!=null){
            this.p1 = new Point(p1.GetAbs(),p1.GetOrd());
        }
    }

    public Point getP2() {
        return new Point(p2);
    }

    public void setP2(Point p2) {
        if(p2!=null){
            this.p2 = new Point(p2.GetAbs(),p2.GetOrd());
        }
    }

    
    public void deplacetoi(Point p1, Point p2)
    { 
        
        this.p1=p1;
        this.p2=p2;
    }
    
    public void deplacetoi(Droite ou)
    {
      if(ou==null) return;  
        this.p1=ou.p1;
        this.p2=ou.p2;
    }
    
    public String toString()
    {  String gne="";
       gne=("Point A = "+p1+" - "+"Point B = "+p2);
       return gne;
    }
    
}
