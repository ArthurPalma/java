/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premierepartiemodule.Geometrie;

/**
 *
 * @author palmaa
 */
public class Point {
    private int abscisse;
    private int ordonnee;
    /**
     * Fonction permettant de déplacer un point en changeant ses coordonnées
     * @param x
     * @param ordonnee 
     */
    public void deplacetoi(int x, int ordonnee)
    {
    abscisse=x;
    
    this.ordonnee=ordonnee;
    }
    /**
     * Fonction permettant de déplacer un point en remplaçant ses coordonnées par celles d'un autre point
     * @param ou 
     */
    public void deplacetoi(Point ou)
    {
      if(ou==null) return;  
        this.abscisse=ou.abscisse;
        this.ordonnee=ou.ordonnee;
    }
    // new instancie, le constructeur initialise
    
    /**
     * Initialise un point avec des coordonnées entrées en paramètres
     * @param abscisse
     * @param ordonnee 
     */
   public Point(int abscisse, int ordonnee)
    {
        // constructeur, retourne rien même pas void
        this.abscisse=abscisse;
        this.ordonnee=ordonnee;
    }
   
   public Point(Point A)
    {
        // constructeur, retourne rien même pas void
        this(A.GetAbs(),A.GetOrd());
        
    }
    
   /**
    * Initialise un point avec des coordonnées initialisées à 0
    */
    public Point()
    {
        // constructeur, retourne rien même pas void
        this(0,0);
        // ci-dessus astuce pour éviter redondance, sert à appeler Point avec coordonnées à 0, évite de réecrire le code, faut que ce soit la première instruction pour que ça marche par contre
    }
// alt + ins
    public int GetAbs() {
        return abscisse;
    }

    public void SetAbs(int abscisse) {
        this.abscisse = abscisse;
    }

    public int GetOrd() {
        return ordonnee;
    }

    public void SetOrd(int ordonnee) {
        this.ordonnee = ordonnee;
    }
    
    
    /*
    public void affichetoi()
    {
    System.out.println("abscisse="+abscisse+" - "+"ordonnee="+ordonnee);
    }
    */
    //écrire exactement comme ça
    /**
     * methode toString
     * @return 
     */
    public String toString()
    {  String gne="";
       gne=("abscisse="+abscisse+" - "+"ordonnee="+ordonnee+"-"+ this.getClass().getSuperclass().getName());
       return gne;
    }
    
    public boolean esTuIdentiqueA(Point lotre)
    {
       if(lotre==null)return false;
       
       if(this==lotre)return true;
    
       return ((abscisse==lotre.abscisse)&&(ordonnee==lotre.ordonnee));
    }
}
