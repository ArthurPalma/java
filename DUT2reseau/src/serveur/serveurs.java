/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serveur;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author palmaa
 */
public class serveurs {

    public static void main(String[] args) {
        ServerSocket serveur = null;
        
        try {            
            serveur = new ServerSocket(1235);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        Socket maSocket = null;

        try {
            maSocket = serveur.accept();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-2);
        }

        InputStream is = null;

        try {
            is = maSocket.getInputStream();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-3);
        }

        BufferedInputStream bis = new BufferedInputStream(is);
        
        try {
            //////////////////////
            for (int i = 0; i < 5; i++) {
                int octetRecu = bis.read();
                System.out.println("L'octet recu est :" + octetRecu);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-4);
        }

        //=====================================================================
        
        OutputStream os = null;

        try {
            os = maSocket.getOutputStream();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-5);
        }
        
        BufferedOutputStream bos = new BufferedOutputStream(os);
        
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println("Octet envoyé");
                bos.write(20 + i);               
            }
            bos.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-5);
        }

        try {
            maSocket.close();
        } catch (IOException ex) {
            
        }
    }
}
