/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author palmaa
 */
public class Client {

    public static void main(String[] args) throws IOException {

        Socket maSocket = null;

        try {
            maSocket = new Socket("127.0.0.1", 1235);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        OutputStream os = null;

        try {
            os = maSocket.getOutputStream();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-2);
        }

        BufferedOutputStream bos = new BufferedOutputStream(os);

        try {
            for (int i = 0; i < 5; i++) {
                bos.write(i);
                System.out.println("octet envoi");

            }
            bos.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-3);
        }

        //=====================================================================
        
        InputStream is = null;

        is=maSocket.getInputStream();
        
        BufferedInputStream bis = new BufferedInputStream(is);        

        try {
            for(int i=0;i<5;i++)
                System.out.println("Octet recu:" + bis.read());
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-3);
        }
                
        try {
            maSocket.close();
        } catch (IOException ex) {

        }
    }

}
